package com.army.vo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class User implements Serializable {

    private static final long serialVersionUID = -8735994241024801682L;

    @Id
//    @GeneratedValue
    private String id;

    @Column(nullable = false, unique = true)
    private String userName;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private int age = 30;

    @Column(nullable = false)
    private Date regTime;

    @Column(nullable = false)
    private String userType;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String state;


}
