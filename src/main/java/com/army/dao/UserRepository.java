package com.army.dao;

import com.army.vo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, String> {
    @Query("select u from User u")
    Page<User> findList(Pageable pageable);

//    User findById(String id);

    User findByUserName(String userName);

    User findByEmail(String email);

    User findByUserNameOrEmail(String userName, String email);

//    void deleteById(String id);
}
