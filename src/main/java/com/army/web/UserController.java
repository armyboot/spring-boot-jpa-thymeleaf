package com.army.web;

import com.army.dao.UserRepository;
import com.army.param.UserParam;
import com.army.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
public class UserController {

    @Resource
    private UserRepository userRepository;

//    @GetMapping("/")
//    public String toList() {
//        return "redirect:/list";
//    }

    @GetMapping("/list")
//    @Cacheable(value = "user_list")
    public String list(Model model, @RequestParam(value = "page", defaultValue = "0") int page,
                       @RequestParam(value = "size", defaultValue = "6") int size) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<User> users = userRepository.findList(pageable);
        model.addAttribute("users", users);
        log.info("user list: " + users.getContent());
        return "user/list";
    }

    @GetMapping("/toAdd")
    public String toAdd() {
        return "user/userAdd";
    }

    @PostMapping("/add")
    public String add(@Valid UserParam userParam, BindingResult result, ModelMap model) {
        String errorMsg = "";
        if (result.hasErrors()) {
            List<ObjectError> list = result.getAllErrors();
            for (ObjectError error : list) {
                errorMsg = errorMsg + error.getCode() + "-" + error.getDefaultMessage() + ";";
            }
            model.addAttribute("errorMsg", errorMsg);
            return "user/userAdd";
        }

        User u = userRepository.findByUserName(userParam.getUserName());
        if (u != null) {
            model.addAttribute("errorMsg", "用户已存在!");
            return "user/userAdd";
        }

        User user = new User();
        BeanUtils.copyProperties(userParam, user);
        user.setId( String.valueOf(new Date().getTime()));
        user.setRegTime(new Date());
        user.setUserType("user");
        user.setState("verified");
        userRepository.save(user);
        return "redirect:/list";
    }

    @GetMapping("/toEdit")
    public String toEdit(Model model, String id) {
        Optional<User> user = userRepository.findById(id);
        model.addAttribute("user", user.get());
        return "user/userEdit";
    }

    @PostMapping("/edit")
    public String edit(@Valid UserParam userParam, BindingResult result, ModelMap model) {
        String errorMsg = "";
        if(result.hasErrors()){
            List<ObjectError> list = result.getAllErrors();
            for (ObjectError error : list) {
                errorMsg = errorMsg + error.getCode() + "-" + error.getDefaultMessage() + ";";
            }
            model.addAttribute("errorMsg", errorMsg);
            model.addAttribute("user", userParam);
            return "user/userEdit";
        }

        User user = new User();
        BeanUtils.copyProperties(userParam, user);
        user.setRegTime(new Date());
        user.setUserType("user");
        user.setState("verified");
        userRepository.save(user);
        return "redirect:/list";
    }

    @GetMapping("/delete")
    public String delete(String id) {
        log.info("Delete ID: " + id);
        userRepository.deleteById(id);
        return "redirect:/list";
    }
}
