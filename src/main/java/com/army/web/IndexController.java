package com.army.web;

import com.army.config.WebConfiguration;
import com.army.dao.UserRepository;
import com.army.param.LoginParam;
import com.army.param.RegisterParam;
import com.army.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
public class IndexController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String from;

    @GetMapping("/")
    public String index(HttpServletRequest request) {
        String id = (String) request.getSession().getAttribute(WebConfiguration.LOGIN_KEY);
        if(StringUtils.isBlank(id)) {
            return "login";
        }else {
            return "redirect:/list";
        }
    }

    @GetMapping("/toLogin")
    public String toLogin(){
        return "login";
    }
    @PostMapping("/login")
    public String login(@Valid LoginParam loginParam, BindingResult result, ModelMap model, HttpServletRequest request){
        if("login".equals(this.redirectLogin(result, model))){
            return "login";
        }

        User user = userRepository.findByUserName(loginParam.getUserName());
        if(null == user){
            user = userRepository.findByEmail(loginParam.getUserName());
        }
        if(null == user){
            model.addAttribute("errorMsg", "用户名不存在!");
            return "login";
        }else if(!user.getPassword().equals(loginParam.getPassword())){
            model.addAttribute("errorMsg", "密码错误!");
            return "login";
        }

        request.getSession().setAttribute(WebConfiguration.LOGIN_KEY, user.getId());
        request.getSession().setAttribute(WebConfiguration.LOGIN_USER, user);

        return "redirect:/list";
    }
    @GetMapping("/loginOut")
    public String loginOut(HttpServletRequest request) {
        request.getSession().removeAttribute(WebConfiguration.LOGIN_KEY);
        request.getSession().removeAttribute(WebConfiguration.LOGIN_USER);

        return "login";
    }

    private String redirectLogin(BindingResult result, ModelMap model) {
        String errorMsg = "";
        if(result.hasErrors()) {
            List<ObjectError> list = result.getAllErrors();
            for(ObjectError error : list) {
                errorMsg += error.getCode() + "-" + error.getDefaultMessage() + ";";
            }
            model.addAttribute("errorMsg", errorMsg);
            return "login";
        }else{
            return "";
        }
    }


    @GetMapping("toRegister")
    public String toRegister() {
        return "register";
    }
    @PostMapping("register")
    public String register(@Valid RegisterParam registerParam, BindingResult result, ModelMap model) {
        log.info("Register Param: " + registerParam.toString());
        if("login".equals(this.redirectLogin(result, model))){
            return "login";
        }

        User u = userRepository.findByUserNameOrEmail(registerParam.getUserName(), registerParam.getEmail());
        if (u != null) {
            model.addAttribute("errorMsg", "用户已存在!");
            return "register";
        }

        User user = new User();
        BeanUtils.copyProperties(registerParam, user);
        user.setRegTime(new Date());
        user.setUserType("manager");
        user.setState("unverified");
        user.setId( String.valueOf(new Date().getTime()));

        log.info(user.toString());
        userRepository.save(user);
        this.sendRegisterMail(user);

        log.info("Register User: " + user.toString());
        return "login";
    }

    private void sendRegisterMail(User user) {
        Context context = new Context();
        context.setVariable("id", user.getId());
        String emailContent = templateEngine.process("emailTemplate", context);
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(user.getEmail());
            helper.setSubject("注册验证邮件");
            helper.setText(emailContent, true);
            helper.setCc(from);
            mailSender.send(message);
            log.info("注册验证邮件已发送到：" + user.getEmail());
        } catch (Exception e) {
            log.error("发送注册邮件时异常！", e);
        }
    }

    @GetMapping("/verified/{id}")
    public String verified(@PathVariable("id") String id, ModelMap model){
        Optional<User> us = userRepository.findById( id );
        User user = us.get();
        if(null != user && "unverified".equals(user.getState())){
            user.setState("verified");
            userRepository.save(user);
            model.put("userName", user.getUserName());
        }
        return "verified";
    }
}
